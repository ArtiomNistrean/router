import data
import path_permutations as pp
import distance_calculator as distance

aux_routings = pp.get_aux_routings(data.routings)


def get_shortest_path(graph, start, end, path=[]):
    path = path + [start]

    if start == end:
        return path

    if start not in graph:
        return None

    shortest = None

    for node in graph[start]:
        if node not in path:
            newpath = get_shortest_path(graph, node, end, path)

            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath

        return shortest


def get_shortest_path_str(path_as_list):

    string = ''

    for node in path_as_list:
        string += node

    return string


if __name__ == "__main__":

    startNode = input('Please Enter a Start: ')
    endNode = input('Please Enter an End: ')

    print("")

    path = get_shortest_path(aux_routings, startNode, endNode)

    path = get_shortest_path_str(path)

    print("Shortest Length: " , distance.get_distance(path))
