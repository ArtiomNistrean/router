from collections import defaultdict
from heapq import *
import numpy as np
import csv

def getGraph(filename):

	graph = []
	with open(filename) as csvfile:
	    readCSV = csv.reader(csvfile, delimiter=',')
	    for row in readCSV:
	   		graph.append(row)

	return graph

def writeGraph(filename, mst):
	with open(filename, 'w') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		writer.writerows(mst)
    	
def getNodes(alist):
	
	nodes = []
	uniqueNodes = []

	for node in alist:
		nodes.append(node[0])
		nodes.append(node[1])

	for node in nodes:
		if node in uniqueNodes:
			pass
		else:
			uniqueNodes.append(node)

	return uniqueNodes

def getEdges(alist):

	edges = []

	for connection in alist:
		connection[2] = int(connection[2])

	for connection in alist:
		edges.append(tuple(connection))

	return edges

 
def minimum_spanning_tree_prims(nodes, edges):
    minimumSpanningTree = []

    connections = defaultdict(list)
    
    for node1, node2, cost in edges:
        connections[node1].append((cost, node1, node2))
        connections[node2].append((cost, node2, node1))
 

    traversed = set(nodes[0])

    unexplored = connections[nodes[0]][:]
    heapify( unexplored )
 
    while unexplored:
        cost, node1, node2 = heappop(unexplored)
        
        if node2 not in traversed:
            traversed.add(node2)
            minimumSpanningTree.append((node1, node2, cost))
 
            for k in connections[node2]:
                if k[2] not in traversed:
                    heappush( unexplored, k )
    
    return minimumSpanningTree
 


if __name__ == '__main__':

	data_graph = '/Users/artiomNistrean/Desktop/mst-example-3.csv'

	graph = getGraph(data_graph)

	list_of_nodes = getNodes(graph)
	list_of_connections = getEdges(graph)

	print(minimum_spanning_tree_prims(list_of_nodes, list_of_connections))

	writeGraph('/Users/artiomNistrean/Desktop/mst-answer.csv', minimum_spanning_tree_prims(list_of_nodes, list_of_connections))



