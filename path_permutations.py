import data


# Gets the number of permutations of a path given a few options

def get_aux_routings(routings):
    aux_routings = {}

    for i in range(len(routings)):
        aux_routings[routings[i][0]] = get_neighbours(routings[i][0], routings)

    return aux_routings


def get_neighbours(node, routings):
    neighbours = []

    for i in range(len(routings)):

        if routings[i][0] == node:
            neighbours.append(routings[i][1])

    return neighbours


def find_all_paths(graph, start, end, path=[]):
    path = path + [start]

    if start == end:
        return [path]

    if not start in graph:
        return []

    paths = []

    for node in graph[start]:

        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)

            for newpath in newpaths:
                paths.append(newpath)

    return paths


def filter_paths(paths, condition, max=True):
    filtered_paths = []

    for path in paths:

        if max:
            if len(path) <= condition:
                filtered_paths.append(path)
        else:
            if len(path) == condition:
                filtered_paths.append(path)

    return filtered_paths


if __name__ == "__main__":

    startNode = input('Please Enter a Start: ')
    endNode = input('Please Enter an End: ')

    print("")

    cond = input('Number of Junctions: ')

    print("")

    cond_specify = input("Exact? (yes / no): ")

    if cond_specify == 'yes':
        cond_specify = False
    else:
        cond_specify = True

    print("")
    aux_routings = get_aux_routings(data.routings)

    if startNode == endNode:
        paths = [[[startNode] + y for y in find_all_paths(aux_routings, x, startNode)] for x in aux_routings[startNode]]
    else:
        paths = find_all_paths(aux_routings, startNode, endNode)

    print("Paths: " , paths)
    print("Number of Path Permutations: " , len(paths))
