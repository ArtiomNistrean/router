

# Responsible for calculating the distance given a path.

print("Loading Data...")
import data
print("Data Loaded! \n")



testRoute1 = 'ABC'
testRoute2 = 'AD'
testRoute3 = 'ADC'
testRoute4 = 'AEBCD'
testRoute5 = 'AED'


def get_distance(path):
    """
    Gets the distance given a string path.

    :param path: The string, uppercase specifiying the path.
    :return: The integer value of the distance of the path or a print string staying there is no such path.
    """
    distance = 0
    expectedCount = len(path) - 1
    count = 0

    for i in range(len(path)):

        for j in range(len(data.routings)):

            if i+1 != len(path):

                if path[i] == data.routings[j][0] and path[i+1] == data.routings[j][1]:
                    distance += data.routings[j][-1]
                    count += 1

    if distance == 0 or count != expectedCount:
        print("NO SUCH PATH!")
        return 0
    else:
        return distance


if __name__ == "__main__":

    path = input('Please Enter a path: ')
    print(get_distance(path))




